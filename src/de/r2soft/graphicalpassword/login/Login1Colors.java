/** ####################################################################### 
 * Copyright 2014 RANDOM ROBOT SOFTWORKS (see @authors file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at*
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ####################################################################### */

package de.r2soft.graphicalpassword.login;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import de.r2soft.graphicalpassword.R;
import de.r2soft.graphicalpassword.core.AppSettings;

/**
 * Star-Fragment for login procedure. User selects colors.
 * 
 * @author Katharina <katharina.sabel@2rsoftworks.de>
 * 
 */
public class Login1Colors extends Fragment implements OnClickListener {
  private ImageView inputOne, inputTwo, inputThree, inputFour;

  private ImageView one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen,
	  fourteen, fifteen, sixteen;

  /** Local copy of all possibly colors to render tiles. */
  private ArrayList<String> colors;

  /** ArrayList with selected colors as Strings */
  private ArrayList<String> selected = new ArrayList<String>() {
	private static final long serialVersionUID = 1779709454402496321L;

	{
	  this.add(null);
	  this.add(null);
	  this.add(null);
	  this.add(null);
	}
  };
  private int index = -1;
  private boolean end = false;

  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View v = inflater.inflate(R.layout.login_1_colors, container, false);
	colors = AppSettings.COLORS;

	inputOne = (ImageView) v.findViewById(R.id.login_input_one);
	inputTwo = (ImageView) v.findViewById(R.id.login_input_two);
	inputThree = (ImageView) v.findViewById(R.id.login_input_three);
	inputFour = (ImageView) v.findViewById(R.id.login_input_four);

	one = (ImageView) v.findViewById(R.id.login_colors_one);
	two = (ImageView) v.findViewById(R.id.login_colors_two);
	three = (ImageView) v.findViewById(R.id.login_colors_three);
	four = (ImageView) v.findViewById(R.id.login_colors_four);
	five = (ImageView) v.findViewById(R.id.login_colors_five);
	six = (ImageView) v.findViewById(R.id.login_colors_six);
	seven = (ImageView) v.findViewById(R.id.login_colors_seven);
	eight = (ImageView) v.findViewById(R.id.login_colors_eight);
	nine = (ImageView) v.findViewById(R.id.login_colors_nine);
	ten = (ImageView) v.findViewById(R.id.login_colors_ten);
	eleven = (ImageView) v.findViewById(R.id.login_colors_eleven);
	twelve = (ImageView) v.findViewById(R.id.login_colors_twelve);
	thirteen = (ImageView) v.findViewById(R.id.login_colors_thirteen);
	fourteen = (ImageView) v.findViewById(R.id.login_colors_fourteen);
	fifteen = (ImageView) v.findViewById(R.id.login_colors_fifteen);
	sixteen = (ImageView) v.findViewById(R.id.login_colors_sixteen);

	one.setOnClickListener(this);
	two.setOnClickListener(this);
	three.setOnClickListener(this);
	four.setOnClickListener(this);
	five.setOnClickListener(this);
	six.setOnClickListener(this);
	seven.setOnClickListener(this);
	eight.setOnClickListener(this);
	nine.setOnClickListener(this);
	ten.setOnClickListener(this);
	eleven.setOnClickListener(this);
	twelve.setOnClickListener(this);
	thirteen.setOnClickListener(this);
	fourteen.setOnClickListener(this);
	fifteen.setOnClickListener(this);
	sixteen.setOnClickListener(this);

	inputOne.setOnClickListener(this);
	inputTwo.setOnClickListener(this);
	inputThree.setOnClickListener(this);
	inputFour.setOnClickListener(this);

	Collections.shuffle(colors); // Comment out if you want static order
	renderColors();
	return v;
  }

  @Override
  public void onClick(View v) {
	// Repositions the index over the data ArrayList.
	for (int n = 0; n <= selected.size() - 1; n++) {
	  if (selected.get(n) == null) {
		index = n;
		break;
	  }
	  else if (n >= selected.size() - 1)
		end = true;
	}
	boolean field = true;
	if (v.equals(inputOne) || v.equals(inputTwo) || v.equals(inputThree) || v.equals(inputFour)) {
	  selected.set(getInputIndex(v), null);
	  field = false;
	}
	else {
	  if (end) {
		Toast.makeText(getActivity(), "Your selection is full. Remove a color before adding a new one",
			Toast.LENGTH_SHORT).show();
		return;
	  }
	  selected.set(index, colors.get(getDisplayIndex(v)));
	}

	updateSelectionFields();
	end = false;

	// // Enable to re-shuffle colors after every selection.
	// if (field) {
	// Collections.shuffle(colors);
	// renderColors();
	// }
  }

  private void updateSelectionFields() {
	inputOne.setColorFilter(AppSettings.getColorWithHex(selected.get(0)));
	inputTwo.setColorFilter(AppSettings.getColorWithHex(selected.get(1)));
	inputThree.setColorFilter(AppSettings.getColorWithHex(selected.get(2)));
	inputFour.setColorFilter(AppSettings.getColorWithHex(selected.get(3)));
  }

  private int getDisplayIndex(View v) {
	switch (v.getId()) {
	case R.id.login_colors_one:
	  return 0;
	case R.id.login_colors_two:
	  return 1;
	case R.id.login_colors_three:
	  return 2;
	case R.id.login_colors_four:
	  return 3;
	case R.id.login_colors_five:
	  return 4;
	case R.id.login_colors_six:
	  return 5;
	case R.id.login_colors_seven:
	  return 6;
	case R.id.login_colors_eight:
	  return 7;
	case R.id.login_colors_nine:
	  return 8;
	case R.id.login_colors_ten:
	  return 9;
	case R.id.login_colors_eleven:
	  return 10;
	case R.id.login_colors_twelve:
	  return 11;
	case R.id.login_colors_thirteen:
	  return 12;
	case R.id.login_colors_fourteen:
	  return 13;
	case R.id.login_colors_fifteen:
	  return 14;
	case R.id.login_colors_sixteen:
	  return 15;

	default:
	  return -1;
	}
  }

  private int getInputIndex(View v) {
	switch (v.getId()) {
	case R.id.login_input_one:
	  return 0;
	case R.id.login_input_two:
	  return 1;
	case R.id.login_input_three:
	  return 2;
	case R.id.login_input_four:
	  return 3;

	default:
	  return -1;
	}
  }

  public ArrayList<String> getSelection() {
	// It can't be anything else
	@SuppressWarnings("unchecked")
	ArrayList<String> temp = (ArrayList<String>) selected.clone();
	clearSelection();
	return temp;
  }

  private void clearSelection() {
	for (int n = 0; n < 4; n++)
	  selected.set(n, null);
  }

  private void renderColors() {
	one.setColorFilter(AppSettings.getColorWithHex(colors.get(0)));
	two.setColorFilter(AppSettings.getColorWithHex(colors.get(1)));
	three.setColorFilter(AppSettings.getColorWithHex(colors.get(2)));
	four.setColorFilter(AppSettings.getColorWithHex(colors.get(3)));
	five.setColorFilter(AppSettings.getColorWithHex(colors.get(4)));
	six.setColorFilter(AppSettings.getColorWithHex(colors.get(5)));
	seven.setColorFilter(AppSettings.getColorWithHex(colors.get(6)));
	eight.setColorFilter(AppSettings.getColorWithHex(colors.get(7)));
	nine.setColorFilter(AppSettings.getColorWithHex(colors.get(8)));
	ten.setColorFilter(AppSettings.getColorWithHex(colors.get(9)));
	eleven.setColorFilter(AppSettings.getColorWithHex(colors.get(10)));
	twelve.setColorFilter(AppSettings.getColorWithHex(colors.get(11)));
	thirteen.setColorFilter(AppSettings.getColorWithHex(colors.get(12)));
	fourteen.setColorFilter(AppSettings.getColorWithHex(colors.get(13)));
	fifteen.setColorFilter(AppSettings.getColorWithHex(colors.get(14)));
	sixteen.setColorFilter(AppSettings.getColorWithHex(colors.get(15)));
  }

  public void flush() {
	clearSelection();
	index = -1;
	end = false;
  }

}
