/** ####################################################################### 
 * Copyright 2014 RANDOM ROBOT SOFTWORKS (see @authors file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at*
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ####################################################################### */

package de.r2soft.graphicalpassword.login;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import de.r2soft.graphicalpassword.R;
import de.r2soft.graphicalpassword.core.AppSettings;
import de.r2soft.graphicalpassword.setup.SetupContainer;

/**
 * Login screen for pictures.
 * 
 * @author Katharina <katharina.sabel@2rsoftworks.de>
 * 
 */
public class Login2Objects extends Fragment implements OnClickListener {
  private ImageView inputOne, inputTwo;

  private ImageView one, two, three, four, five, six, seven, eight;

  private ArrayList<String> objects;
  private ArrayList<String> colorBatch;
  private ArrayList<Pair<String, String>> batchedObjects;
  private ArrayList<String> keys;
  private ArrayList<String> values;

  /** ArrayList with selected colors as Strings */
  private ArrayList<String> selected = new ArrayList<String>() {
	private static final long serialVersionUID = 1779709454402496321L;

	{
	  this.add(null);
	  this.add(null);
	}
  };

  private ArrayList<String> colors = new ArrayList<String>() {
	private static final long serialVersionUID = 6579058111280439612L;

	{
	  this.add(null);
	  this.add(null);
	}
  };
  private int index = -1;
  private boolean end = false;

  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View v = inflater.inflate(R.layout.login_2_objects, container, false);
	objects = AppSettings.PICTURES;
	colorBatch = AppSettings.COLORS;
	Collections.shuffle(colorBatch);

	if (AppSettings.getBigBatchFlag(getActivity())) {
	  batchedObjects = AppSettings.getBigBatchMap(getActivity());
	  keys = new ArrayList<String>();
	  values = new ArrayList<String>();
	  for (Pair<String, String> entry : batchedObjects) {
		keys.add(entry.first);
		values.add(entry.second);
	  }
	}

	System.out.println(keys);
	System.out.println(values);

	inputOne = (ImageView) v.findViewById(R.id.login_obj_input_one);
	inputTwo = (ImageView) v.findViewById(R.id.login_obj_input_two);

	one = (ImageView) v.findViewById(R.id.login_object_one);
	two = (ImageView) v.findViewById(R.id.login_object_two);
	three = (ImageView) v.findViewById(R.id.login_object_three);
	four = (ImageView) v.findViewById(R.id.login_object_four);
	five = (ImageView) v.findViewById(R.id.login_object_five);
	six = (ImageView) v.findViewById(R.id.login_object_six);
	seven = (ImageView) v.findViewById(R.id.login_object_seven);
	eight = (ImageView) v.findViewById(R.id.login_object_eight);

	one.setOnClickListener(this);
	two.setOnClickListener(this);
	three.setOnClickListener(this);
	four.setOnClickListener(this);
	five.setOnClickListener(this);
	six.setOnClickListener(this);
	seven.setOnClickListener(this);
	eight.setOnClickListener(this);

	inputOne.setOnClickListener(this);
	inputTwo.setOnClickListener(this);

	renderImages();

	return v;
  }

  private void renderImages() {
	if (AppSettings.getBigBatchFlag(getActivity())) {
	  batchedObjects = AppSettings.getBigBatchMap(getActivity());

	  one.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(0)));
	  one.setBackgroundColor(AppSettings.getColorWithHex(values.get(0)));

	  two.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(1)));
	  two.setBackgroundColor(AppSettings.getColorWithHex(values.get(1)));

	  three.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(2)));
	  three.setBackgroundColor(AppSettings.getColorWithHex(values.get(2)));

	  four.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(3)));
	  four.setBackgroundColor(AppSettings.getColorWithHex(values.get(3)));

	  five.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(4)));
	  five.setBackgroundColor(AppSettings.getColorWithHex(values.get(4)));

	  six.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(5)));
	  six.setBackgroundColor(AppSettings.getColorWithHex(values.get(5)));

	  seven.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(6)));
	  seven.setBackgroundColor(AppSettings.getColorWithHex(values.get(6)));

	  eight.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(7)));
	  eight.setBackgroundColor(AppSettings.getColorWithHex(values.get(7)));

	}
	else {	  // Every day I'm shuffling! :)
	  Collections.shuffle(objects);

	  one.setImageResource(AppSettings.getPictureDrawableWithStringId(objects.get(0)));
	  one.setBackgroundColor(AppSettings.getColorWithHex(colorBatch.get(0)));

	  two.setImageResource(AppSettings.getPictureDrawableWithStringId(objects.get(1)));
	  two.setBackgroundColor(AppSettings.getColorWithHex(colorBatch.get(1)));

	  three.setImageResource(AppSettings.getPictureDrawableWithStringId(objects.get(2)));
	  three.setBackgroundColor(AppSettings.getColorWithHex(colorBatch.get(2)));

	  four.setImageResource(AppSettings.getPictureDrawableWithStringId(objects.get(3)));
	  four.setBackgroundColor(AppSettings.getColorWithHex(colorBatch.get(3)));

	  five.setImageResource(AppSettings.getPictureDrawableWithStringId(objects.get(4)));
	  five.setBackgroundColor(AppSettings.getColorWithHex(colorBatch.get(4)));

	  six.setImageResource(AppSettings.getPictureDrawableWithStringId(objects.get(5)));
	  six.setBackgroundColor(AppSettings.getColorWithHex(colorBatch.get(5)));

	  seven.setImageResource(AppSettings.getPictureDrawableWithStringId(objects.get(6)));
	  seven.setBackgroundColor(AppSettings.getColorWithHex(colorBatch.get(6)));

	  eight.setImageResource(AppSettings.getPictureDrawableWithStringId(objects.get(7)));
	  eight.setBackgroundColor(AppSettings.getColorWithHex(colorBatch.get(7)));
	}

  }

  @Override
  public void onClick(View v) {
	// Repositions the index over the data ArrayList.
	for (int n = 0; n <= selected.size() - 1; n++) {
	  if (selected.get(n) == null) {
		index = n;
		break;
	  }
	  else if (n >= selected.size() - 1)
		end = true;
	}
	if (v.equals(inputOne) || v.equals(inputTwo)) {
	  selected.set(getInputIndex(v), null);
	  colors.set(getInputIndex(v), null);
	}
	else {
	  if (end) {
		Toast.makeText(getActivity(), "Your selection is full. Remove a picture before adding a new one",
			Toast.LENGTH_SHORT).show();
		return;
	  }
	  if (SetupContainer.bigBatchWriting) {
		selected.set(index, keys.get(getDisplayIndex(v)));
		colors.set(index, values.get(getDisplayIndex(v)));
	  }
	  else {
		selected.set(index, objects.get(getDisplayIndex(v)));
		colors.set(index, colorBatch.get(getDisplayIndex(v) + 1));
	  }
	}
	updateSelectionFields();
	end = false;
  }

  private void updateSelectionFields() {
	if (selected.get(0) != null) {
	  inputOne.setImageResource(AppSettings.getPictureDrawableWithStringId(selected.get(0)));
	  inputOne.setBackgroundColor(AppSettings.getColorWithHex(colors.get(0)));
	}
	else {
	  inputOne.setImageResource(R.drawable.r2_graph_container);
	  inputOne.setBackgroundColor(0);
	}

	if (selected.get(1) != null) {
	  inputTwo.setImageResource(AppSettings.getPictureDrawableWithStringId(selected.get(1)));
	  inputTwo.setBackgroundColor(AppSettings.getColorWithHex(colors.get(1)));
	}
	else {
	  inputTwo.setImageResource(R.drawable.r2_graph_container);
	  inputTwo.setBackgroundColor(0);
	}
  }

  public ArrayList<String> getSelection() {
	// It can't be anything else
	@SuppressWarnings("unchecked")
	ArrayList<String> temp = (ArrayList<String>) selected.clone();
	clearSelection();
	return temp;
  }

  private void clearSelection() {
	for (int n = 0; n < 2; n++)
	  selected.set(n, null);
	for (int n = 0; n < 2; n++)
	  colors.set(n, null);
  }

  public void flush() {
	clearSelection();
	index = -1;
	end = false;
  }

  private int getDisplayIndex(View v) {
	switch (v.getId()) {
	case R.id.login_object_one:
	  return 0;
	case R.id.login_object_two:
	  return 1;
	case R.id.login_object_three:
	  return 2;
	case R.id.login_object_four:
	  return 3;
	case R.id.login_object_five:
	  return 4;
	case R.id.login_object_six:
	  return 5;
	case R.id.login_object_seven:
	  return 6;
	case R.id.login_object_eight:
	  return 7;

	default:
	  return -1;
	}
  }

  private int getInputIndex(View v) {
	switch (v.getId()) {
	case R.id.login_obj_input_one:
	  return 0;
	case R.id.login_obj_input_two:
	  return 1;

	default:
	  return -1;
	}
  }

}
