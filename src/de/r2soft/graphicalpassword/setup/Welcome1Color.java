/** ####################################################################### 
 * Copyright 2014 RANDOM ROBOT SOFTWORKS (see @authors file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at*
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ####################################################################### */
package de.r2soft.graphicalpassword.setup;

import java.util.ArrayList;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import de.r2soft.graphicalpassword.R;
import de.r2soft.graphicalpassword.core.AppSettings;

/**
 * Enables user to enter colors.
 * 
 * @author Katharina <katharina.sabel@2rsoftworks.de>
 * 
 */
public class Welcome1Color extends Fragment implements OnClickListener {
  private ImageView one, two, three, four, five, six, seven, eight;
  private ImageView inputOne, inputTwo, inputThree, inputFour;

  /** ArrayList with selected colors as Strings */
  private ArrayList<String> selected = new ArrayList<String>() {
	private static final long serialVersionUID = 1779709454402496321L;

	{
	  this.add(null);
	  this.add(null);
	  this.add(null);
	  this.add(null);
	}
  };
  private int index = -1;
  private boolean end = false;

  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View v = inflater.inflate(R.layout.dialog_welcome_subscreen_1, container, false);

	one = (ImageView) v.findViewById(R.id.setup_color_r2_view_one);
	two = (ImageView) v.findViewById(R.id.setup_color_r2_view_two);
	three = (ImageView) v.findViewById(R.id.setup_color_r2_view_three);
	four = (ImageView) v.findViewById(R.id.setup_color_r2_view_four);
	five = (ImageView) v.findViewById(R.id.setup_color_r2_view_five);
	six = (ImageView) v.findViewById(R.id.setup_color_r2_view_six);
	seven = (ImageView) v.findViewById(R.id.setup_color_r2_view_seven);
	eight = (ImageView) v.findViewById(R.id.setup_color_r2_view_eight);

	inputOne = (ImageView) v.findViewById(R.id.setup_color_input_one);
	inputTwo = (ImageView) v.findViewById(R.id.setup_color_input_two);
	inputThree = (ImageView) v.findViewById(R.id.setup_color_input_three);
	inputFour = (ImageView) v.findViewById(R.id.setup_color_input_four);

	one.setOnClickListener(this);
	two.setOnClickListener(this);
	three.setOnClickListener(this);
	four.setOnClickListener(this);
	five.setOnClickListener(this);
	six.setOnClickListener(this);
	seven.setOnClickListener(this);
	eight.setOnClickListener(this);

	inputOne.setOnClickListener(this);
	inputTwo.setOnClickListener(this);
	inputThree.setOnClickListener(this);
	inputFour.setOnClickListener(this);

	one.setColorFilter(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(0)));
	two.setColorFilter(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(1)));
	three.setColorFilter(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(2)));
	four.setColorFilter(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(3)));
	five.setColorFilter(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(4)));
	six.setColorFilter(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(5)));
	seven.setColorFilter(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(6)));
	eight.setColorFilter(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(7)));

	/** Will restore a selection made in a previous screen from the database. */
	buildSelection();
	updateSelectionFields();
	return v;
  }

  /**
   * The overridden method of the OnClickListener interface. Will check the data index on the
   * {@link #selected} ArrayList to locate data entry point.
   * 
   * @author Katharina <katharina.sabel@2rsoftworks.de>
   * 
   * @param v
   *          The view that triggered the onClick.
   */
  @Override
  public void onClick(View v) {
	// Repositions the index over the data ArrayList.
	for (int n = 0; n <= selected.size() - 1; n++) {
	  if (selected.get(n) == null) {
		index = n;
		break;
	  }
	  else if (n >= selected.size() - 1)
		end = true;
	}

	if (v.equals(inputOne) || v.equals(inputTwo) || v.equals(inputThree) || v.equals(inputFour)) {
	  selected.set(getInputIndex(v), null);
	}
	else {
	  if (end) {
		Toast.makeText(getActivity(), "Your selection is full. Remove a color before adding a new one",
			Toast.LENGTH_SHORT).show();
		return;
	  }
	  selected.set(index, SetupContainer.filteredColors.get(getDisplayIndex(v)));
	}

	updateSelectionFields();
	end = false;
  }

  private void updateSelectionFields() {
	inputOne.setColorFilter(AppSettings.getColorWithHex(selected.get(0)));
	inputTwo.setColorFilter(AppSettings.getColorWithHex(selected.get(1)));
	inputThree.setColorFilter(AppSettings.getColorWithHex(selected.get(2)));
	inputFour.setColorFilter(AppSettings.getColorWithHex(selected.get(3)));
  }

  private int getDisplayIndex(View v) {
	switch (v.getId()) {
	case R.id.setup_color_r2_view_one:
	  return 0;
	case R.id.setup_color_r2_view_two:
	  return 1;
	case R.id.setup_color_r2_view_three:
	  return 2;
	case R.id.setup_color_r2_view_four:
	  return 3;
	case R.id.setup_color_r2_view_five:
	  return 4;
	case R.id.setup_color_r2_view_six:
	  return 5;
	case R.id.setup_color_r2_view_seven:
	  return 6;
	case R.id.setup_color_r2_view_eight:
	  return 7;

	default:
	  return -1;
	}
  }

  private int getInputIndex(View v) {
	switch (v.getId()) {
	case R.id.setup_color_input_one:
	  return 0;
	case R.id.setup_color_input_two:
	  return 1;
	case R.id.setup_color_input_three:
	  return 2;
	case R.id.setup_color_input_four:
	  return 3;

	default:
	  return -1;
	}
  }

  public void finalizeSelection() {
	AppSettings.setColors(selected, getActivity());
  }

  /**
   * Rebuilds the selection that was previously made when clicking the back-button the Setup Agent.
   */
  public void buildSelection() {
	selected = AppSettings.getColors(getActivity());
  }
}
