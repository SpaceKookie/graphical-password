/** ####################################################################### 
 * Copyright 2014 RANDOM ROBOT SOFTWORKS (see @authors file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at*
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ####################################################################### */
package de.r2soft.graphicalpassword.setup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import de.r2soft.graphicalpassword.R;
import de.r2soft.graphicalpassword.core.AppSettings;

/**
 * Fragment for user to select pictures.
 * 
 * @author Katharina <katharina.sabel@2rsoftworks.de>
 * 
 */
public class Welcome2Obj extends Fragment implements OnClickListener {

  private ImageView one, two, three, four;
  private ImageView selectOne, selectTwo;

  private ArrayList<Pair<String, String>> batchedObjects;
  private ArrayList<String> keys;
  private ArrayList<String> values;

  /** ArrayList with selected colors as Strings */
  private ArrayList<String> selected = new ArrayList<String>() {
	private static final long serialVersionUID = 1779709454402496321L;

	{
	  this.add(null);
	  this.add(null);
	}
  };

  /**
   * Temporary list to hold color information for the selection. This is only for rendering
   * purposes. Data will NOT be written into SharedPreferences Database.
   */
  private ArrayList<String> colors = new ArrayList<String>() {
	private static final long serialVersionUID = -5133051243835466831L;

	{
	  this.add(null);
	  this.add(null);
	}
  };
  private int index = -1;
  private boolean end = false;

  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View v = inflater.inflate(R.layout.dialog_welcome_subscreen_2, container, false);

	one = (ImageView) v.findViewById(R.id.setup_obj_r2_view_one);
	two = (ImageView) v.findViewById(R.id.setup_obj_r2_view_two);
	three = (ImageView) v.findViewById(R.id.setup_obj_r2_view_three);
	four = (ImageView) v.findViewById(R.id.setup_obj_r2_view_four);

	selectOne = (ImageView) v.findViewById(R.id.setup_obj_input_one);
	selectTwo = (ImageView) v.findViewById(R.id.setup_obj_input_two);

	one.setOnClickListener(this);
	two.setOnClickListener(this);
	three.setOnClickListener(this);
	four.setOnClickListener(this);

	selectOne.setOnClickListener(this);
	selectTwo.setOnClickListener(this);

	if (!SetupContainer.bigBatchWriting) {
	  one.setImageResource(AppSettings.getPictureDrawableWithStringId(SetupContainer.filteredObjects.get(0)));
	  two.setImageResource(AppSettings.getPictureDrawableWithStringId(SetupContainer.filteredObjects.get(1)));
	  three
		  .setImageResource(AppSettings.getPictureDrawableWithStringId(SetupContainer.filteredObjects.get(2)));
	  four.setImageResource(AppSettings.getPictureDrawableWithStringId(SetupContainer.filteredObjects.get(3)));

	  one.setBackgroundColor(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(1)));
	  two.setBackgroundColor(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(2)));
	  three.setBackgroundColor(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(3)));
	  four.setBackgroundColor(AppSettings.getColorWithHex(SetupContainer.filteredColors.get(4)));
	}
	else {
	  /* THIS WILL BE CALLED IF BIGBATWRITING IS TRUE! */
	  batchedObjects = AppSettings.getBigBatchMap(getActivity());
	  keys = new ArrayList<String>();
	  values = new ArrayList<String>();
	  for (Pair<String, String> entry : batchedObjects) {
		keys.add(entry.first);
		values.add(entry.second);
	  }

	  // Use different values if needed
	  one.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(0)));
	  two.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(1)));
	  three.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(2)));
	  four.setImageResource(AppSettings.getPictureDrawableWithStringId(keys.get(3)));

	  one.setBackgroundColor(AppSettings.getColorWithHex(values.get(0)));
	  two.setBackgroundColor(AppSettings.getColorWithHex(values.get(1)));
	  three.setBackgroundColor(AppSettings.getColorWithHex(values.get(2)));
	  four.setBackgroundColor(AppSettings.getColorWithHex(values.get(3)));
	}
	buildSelection();
	updateSelectionFields();

	return v;
  }

  /** Writes selection to DB */
  public void finalizeSelection() {
	AppSettings.setObjects(selected, getActivity());
	AppSettings.setSmallBatchColors(colors, getActivity());
  }

  /**
   * Rebuilds the selection that was previously made when clicking the back-button the Setup Agent.
   */
  public void buildSelection() {
	selected = AppSettings.getObjects(getActivity());
	colors = AppSettings.getSmallBatchColors(getActivity());
  }

  @Override
  public void onClick(View v) {
	// Repositions the index over the data ArrayList.
	for (int n = 0; n <= selected.size() - 1; n++) {
	  if (selected.get(n) == null) {
		index = n;
		break;
	  }
	  else if (n >= selected.size() - 1)
		end = true;
	}
	if (v.equals(selectOne) || v.equals(selectTwo)) {
	  selected.set(getInputIndex(v), null);
	  colors.set(getInputIndex(v), null);
	}
	else {
	  if (end) {
		Toast.makeText(getActivity(), "Your selection is full. Remove a picture before adding a new one",
			Toast.LENGTH_SHORT).show();
		return;
	  }
	  if (SetupContainer.bigBatchWriting) {
		selected.set(index, keys.get(getDisplayIndex(v)));
		colors.set(index, values.get(getDisplayIndex(v)));
	  }
	  else {
		selected.set(index, SetupContainer.filteredObjects.get(getDisplayIndex(v)));
		colors.set(index, SetupContainer.filteredColors.get(getDisplayIndex(v) + 1));
	  }
	}
	updateSelectionFields();
	end = false;
  }

  private void updateSelectionFields() {
	if (selected.get(0) != null) {
	  selectOne.setImageResource(AppSettings.getPictureDrawableWithStringId(selected.get(0)));
	  selectOne.setBackgroundColor(AppSettings.getColorWithHex(colors.get(0)));
	}
	else {
	  selectOne.setImageResource(R.drawable.r2_graph_container);
	  selectOne.setBackgroundColor(0);
	}

	if (selected.get(1) != null) {
	  selectTwo.setImageResource(AppSettings.getPictureDrawableWithStringId(selected.get(1)));
	  selectTwo.setBackgroundColor(AppSettings.getColorWithHex(colors.get(1)));
	}
	else {
	  selectTwo.setImageResource(R.drawable.r2_graph_container);
	  selectTwo.setBackgroundColor(0);
	}
  }

  private int getDisplayIndex(View v) {
	switch (v.getId()) {
	case R.id.setup_obj_r2_view_one:
	  return 0;
	case R.id.setup_obj_r2_view_two:
	  return 1;
	case R.id.setup_obj_r2_view_three:
	  return 2;
	case R.id.setup_obj_r2_view_four:
	  return 3;

	default:
	  return -1;
	}
  }

  private int getInputIndex(View v) {
	switch (v.getId()) {
	case R.id.setup_obj_input_one:
	  return 0;
	case R.id.setup_obj_input_two:
	  return 1;

	default:
	  return -1;
	}
  }

}