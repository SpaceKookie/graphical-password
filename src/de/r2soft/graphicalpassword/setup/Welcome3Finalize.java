/** ####################################################################### 
 * Copyright 2014 RANDOM ROBOT SOFTWORKS (see @authors file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at*
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ####################################################################### */
package de.r2soft.graphicalpassword.setup;

import java.util.ArrayList;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import de.r2soft.graphicalpassword.R;
import de.r2soft.graphicalpassword.core.AppSettings;

/**
 * Final fragment to show result
 * 
 * @author Katharina <katharina.sabel@2rsoftworks.de>
 * 
 */
public class Welcome3Finalize extends Fragment {
  private ImageView colorOne, colorTwo, colorThree, colorFour;
  private ImageView objectOne, objectTwo;

  /** Local instance of db colors ArrayList */
  private ArrayList<String> colors;

  /** Local instance of db objects ArrayList */
  private ArrayList<String> objects;

  /** Local instance of db background color ArrayList */
  private ArrayList<String> bgColors;

  /**
   * Need to be checked by the {@link SetupContainer} to determine if User can advance to the
   * Usage-Screens and close {@link SetupContainer} activity tree
   */
  public boolean colorsComplete = false, objectsComplete = false;

  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	View v = inflater.inflate(R.layout.dialog_welcome_subscreen_3, container, false);

	colorOne = (ImageView) v.findViewById(R.id.setup_sum_color_one);
	colorTwo = (ImageView) v.findViewById(R.id.setup_sum_color_two);
	colorThree = (ImageView) v.findViewById(R.id.setup_sum_color_three);
	colorFour = (ImageView) v.findViewById(R.id.setup_sum_color_four);

	objectOne = (ImageView) v.findViewById(R.id.setup_sum_obj_one);
	objectTwo = (ImageView) v.findViewById(R.id.setup_sum_obj_two);

	updateView();
	return v;
  }

  /**
   * Will call when the fragment is re-attached to an activity and visible to the user. This is the
   * queue to update the view and db instances of the {@link #colors} and {@link #objects}
   * ArrayLists.
   * 
   */
  @Override
  public void onResume() {
	super.onResume();
	updateView();
	((SetupContainer) getActivity()).updateView();
  }

  /** Called on each {@link onResume} */
  public void updateView() {
	buildColors();
	buildObjects();
  }

  private void buildColors() {
	colors = AppSettings.getColors(getActivity());

	for (String s : colors) {
	  if (s == null) {
		colorsComplete = false;
		Toast.makeText(getActivity(), "You haven't selected sufficient colors to continue",
			Toast.LENGTH_SHORT).show();
		drawColors();
		return;
	  }
	}
	colorsComplete = true;
	drawColors();
  }

  private void drawColors() {
	colorOne.setColorFilter(AppSettings.getColorWithHex(colors.get(0)));
	colorTwo.setColorFilter(AppSettings.getColorWithHex(colors.get(1)));
	colorThree.setColorFilter(AppSettings.getColorWithHex(colors.get(2)));
	colorFour.setColorFilter(AppSettings.getColorWithHex(colors.get(3)));
  }

  private void buildObjects() {
	objects = AppSettings.getObjects(getActivity());
	bgColors = AppSettings.getSmallBatchColors(getActivity());

	for (String s : objects) {
	  if (s == null) {
		objectsComplete = false;
		Toast.makeText(getActivity(), "You haven't selected sufficient pictures to continue",
			Toast.LENGTH_SHORT).show();
		drawObjects();
		return;
	  }
	}
	objectsComplete = true;
	drawObjects();
  }

  private void drawObjects() {
	if (objects.get(0) != null) {
	  objectOne.setImageResource(AppSettings.getPictureDrawableWithStringId(objects.get(0)));
	  objectOne.setBackgroundColor(AppSettings.getColorWithHex(bgColors.get(0)));
	}

	if (objects.get(1) != null) {
	  objectTwo.setImageResource(AppSettings.getPictureDrawableWithStringId(objects.get(1)));
	  objectTwo.setBackgroundColor(AppSettings.getColorWithHex(bgColors.get(1)));
	}
  }

}