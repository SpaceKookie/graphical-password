/** ####################################################################### 
 * Copyright 2014 RANDOM ROBOT SOFTWORKS (see @authors file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at*
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ####################################################################### */

package de.r2soft.graphicalpassword.setup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import de.r2soft.graphicalpassword.R;
import de.r2soft.graphicalpassword.core.AppSettings;
import de.r2soft.graphicalpassword.core.CoreActivity;

/**
 * Container activity to display and handle Setup_sub_fragments and write hashed color and picture
 * codes to db.
 * 
 * @author Katharina <katharina.sabel@2rsoftworks.de>
 * 
 */
public class SetupContainer extends Activity {

  public static ArrayList<String> filteredColors;
  public static ArrayList<String> filteredObjects;

  private enum STATE {
	INIT, COLOR, OBJECT, FINAL;
  }

  private STATE state;
  private TextView textview;
  private Button leftNavigation, rightNavigation;
  private Welcome1Color colorFragment;
  private Welcome2Obj objFragment;
  private Welcome3Finalize finalizeFragment;

  @Override
  protected void onCreate(Bundle states) {
	super.onCreate(states);
	AppSettings.setBigBatchFlag(bigBatchWriting, this);
	setContentView(R.layout.dialog_welcome_container);
	state = STATE.INIT;
	colorFragment = new Welcome1Color();
	objFragment = new Welcome2Obj();
	finalizeFragment = new Welcome3Finalize();

	initRandomColors();
	initRandomObjects();

	/* Setup navigational buttons */
	leftNavigation = (Button) findViewById(R.id.welcome_screen_left_button);
	leftNavigation.setOnClickListener(leftButtonClickListener);
	rightNavigation = (Button) findViewById(R.id.welcome_screen_right_button);
	rightNavigation.setOnClickListener(rightButtonClickListener);

	getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);

	showFragment(state);

  }

  private OnClickListener leftButtonClickListener = new OnClickListener() {
	@Override
	public void onClick(View v) {
	  if (state == STATE.COLOR)
		showFragment(STATE.INIT);

	  else if (state == STATE.OBJECT)
		showFragment(STATE.COLOR);

	  else if (state == STATE.FINAL)
		showFragment(STATE.OBJECT);
	}
  };

  private OnClickListener rightButtonClickListener = new OnClickListener() {
	@Override
	public void onClick(View v) {
	  if (state == STATE.INIT)
		showFragment(STATE.COLOR);
	  else if (state == STATE.COLOR)
		showFragment(STATE.OBJECT);
	  else if (state == STATE.OBJECT)
		showFragment(STATE.FINAL);
	  else if (state == STATE.FINAL) {
		Intent intent = new Intent(getApplicationContext(), CoreActivity.class);
		startActivity(intent);
		getFragmentManager().popBackStack();
		AppSettings.setInitState(true, getApplicationContext());
		Toast.makeText(getApplicationContext(), "The application is now configured and ready for use.",
			Toast.LENGTH_SHORT).show();
	  }
	}
  };

  @Override
  public void onDestroy() {
	super.onDestroy();
	finish();
  }

  public void updateView() {
	if (state == STATE.INIT) {
	  leftNavigation.setEnabled(false);
	  rightNavigation.setEnabled(true);
	  leftNavigation.setText("Cancel");
	  rightNavigation.setText("Start Setup");
	}
	else if (state == STATE.COLOR || state == STATE.OBJECT) {
	  leftNavigation.setEnabled(true);
	  leftNavigation.setText("Back");
	  rightNavigation.setText("Next");
	  rightNavigation.setEnabled(true);
	}
	else if (state == STATE.FINAL) {
	  leftNavigation.setText("Back");
	  rightNavigation.setText("Finish");

	  if (finalizeFragment.colorsComplete && finalizeFragment.objectsComplete)
		rightNavigation.setEnabled(true);
	  else
		rightNavigation.setEnabled(false);
	}
  }

  private void showFragment(STATE state) {

	/** Writes selection to DB */
	if (this.state == STATE.COLOR) {
	  colorFragment.finalizeSelection();
	}

	/** Writes picture selection to DB */
	if (this.state == STATE.OBJECT) {
	  objFragment.finalizeSelection();
	}

	FragmentTransaction trans = getFragmentManager().beginTransaction();

	if (state == STATE.INIT) {
	  trans.replace(R.id.welcome_screen_fragment_container, new Welcome0Init());
	}
	else if (state == STATE.COLOR) {
	  trans.replace(R.id.welcome_screen_fragment_container, colorFragment);
	}
	else if (state == STATE.OBJECT) {
	  trans.replace(R.id.welcome_screen_fragment_container, objFragment);
	}
	else if (state == STATE.FINAL) {
	  trans.replace(R.id.welcome_screen_fragment_container, finalizeFragment);
	}
	else {
	  Log.e(getClass().getSimpleName(), "INVALID STATE DETECTED!");
	}

	this.state = state;
	trans.commit();
	updateView();
  }

  /*
   * ##### USAGE GUIDE #####
   * 
   * The following two methods will get a selection of items from the Database for the user to use
   * during creation. Now there are several things you can do with this:
   * 
   * 1. Set collectionShuffle to false to ensure that each user gets the same colors and pictures.
   * Alternatively you can also comment out a line to overwrite the flag.
   * 
   * 2. Set selective = true to only select very specific colors. In the default case I selected 8
   * colors that are very distinctive and 4 objects that have nothing in common with each other.
   * 
   * 3. In the case of the pictures you can enable "bigBatchWriting" to enable the writing of the
   * picture-color combinations as hard-coded entities into the database. The login screen will then
   * start working with a Map instead of a List to shuffle colors and pictures.
   */
  public static boolean selective = true;
  public static boolean bigBatchWriting = true;
  public static boolean collectionShuffle = false;

  /**
   * Generates random colors to work on for the set-up Change this to set a fixed set of colors to
   * be selected in the setup screen.
   */
  private void initRandomColors() {
	filteredColors = new ArrayList<String>();
	ArrayList<String> temp = AppSettings.COLORS;
	if (collectionShuffle)
	  Collections.shuffle(temp); // Comment to overwrite flag

	if (selective) {
	  // Red, brown, purple, light-blue, lime, orange, black, teal
	  int[] selects = new int[] { 0, 12, 4, 5, 8, 11, 14, 7 };
	  for (int n = 0; n < selects.length; n++)
		filteredColors.add(temp.get(selects[n]));
	}

	for (int n = 0; n <= 8; n++) {
	  filteredColors.add(temp.get(n));
	}

	AppSettings.setSmallBatchColors(filteredColors, this);
  }

  private void initRandomObjects() {
	filteredObjects = new ArrayList<String>();
	ArrayList<String> pics = AppSettings.PICTURES;
	ArrayList<String> cols = AppSettings.COLORS;

	if (collectionShuffle) {
	  Collections.shuffle(pics); // Comment out to overwrite flag
	  Collections.shuffle(cols); // Comment out to overwrite flag
	}

	if (bigBatchWriting) {
	  if (collectionShuffle)
		Log.e(getClass().getSimpleName(), "BigBatchWriting is not recommended with random colors/ objects");
	  ArrayList<Pair<String, String>> temp2 = new ArrayList<Pair<String, String>>();

	  /*
	   * HERE YOU CAN SPECIFY THE PICTURE - COLOR COMBINATIONS YOU WANT FOR YOUR TESTS. THE SAME
	   * INDICES FROM BOTH ARRAYS WILL BE USED.
	   */
	  List<Integer> specPics = Arrays.asList(new Integer[] { 0, 1, 2, 3, 4, 5, 6, 7 });
	  // Collections.shuffle(specPics);

	  for (int n = 0; n < 8; n++) {
		temp2.add(new Pair<String, String>(pics.get(specPics.get(n)), filteredColors.get(n)));
	  }
	  Collections.shuffle(temp2); // One shuffle to init random the list.
	  AppSettings.setBigBatchMap(temp2, this);
	  Log.i(getClass().getSimpleName(), "Batch was written to DB. Be sure to actually use it later on!");
	  return;
	}

	if (selective) {
	  // Car, Hat (Fedora), Phone, Shoe, Umbrella
	  int[] selects = new int[] { 0, 1, 4, 7, };
	  for (int n = 0; n < selects.length; n++)
		filteredObjects.add(pics.get(selects[n]));
	}
	else {
	  for (int n = 0; n <= 4; n++) {
		filteredObjects.add(pics.get(n));
	  }
	}

  }
}
