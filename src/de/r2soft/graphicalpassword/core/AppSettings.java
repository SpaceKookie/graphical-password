/** ####################################################################### 
 * Copyright 2014 RANDOM ROBOT SOFTWORKS (see @authors file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at*
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ####################################################################### */
package de.r2soft.graphicalpassword.core;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.util.Pair;
import de.r2soft.graphicalpassword.R;

/**
 * DB backend and a bunch of values to be used throughout the application as static references.
 * 
 * @author Katharina <katharina.sabel@2rsoftworks.de>
 * 
 */
public class AppSettings {

  /** A list of colors that the application will use. Can either be shuffled or used in order. */
  public static final ArrayList<String> COLORS = new ArrayList<String>() {
	private static final long serialVersionUID = 6458837343367627195L;

	{
	  /** Light Red */
	  this.add("FF1C1C"); // 0

	  /** Dark Red */
	  this.add("8A1616"); // 1

	  /** Light Pink */
	  this.add("FF00DD"); // 2

	  /** Dark Pink */
	  this.add("AD009F"); // 3

	  /** Purple */
	  this.add("AE00FF"); // 4

	  /** Light Blue */
	  this.add("2474FF"); // 5

	  /** Dark Blue */
	  this.add("0025CC"); // 6

	  /** Teal */
	  this.add("00E1FF"); // 7

	  /** Light Green */
	  this.add("52FF69"); // 8

	  /** Dark Green */
	  this.add("02AD19"); // 9

	  /** Yellow */
	  this.add("EAFF00"); // 10

	  /** Orange */
	  this.add("FFBB00"); // 11

	  /** Brown */
	  this.add("9C6327"); // 12

	  /** Grey */
	  this.add("808080"); // 13

	  /** Black */
	  this.add("121212"); // 14

	  /** White */
	  this.add("F2F2F2"); // 15
	}
  };

  /**
   * A list of picture objects (ID Strings) to use by the application. Can be shuffled or used in
   * order.
   */
  public static final ArrayList<String> PICTURES = new ArrayList<String>() {
	private static final long serialVersionUID = 5891960305562484547L;

	{
	  this.add("Car"); // 0

	  this.add("Hat"); // 1

	  this.add("Hat2"); // 2

	  this.add("Laptop"); // 3

	  this.add("Phone"); // 4

	  this.add("Plane"); // 5

	  this.add("Shoe"); // 6

	  this.add("Umbrella"); // 7
	}
  };

  /** Checks if the app has been initialized */
  public static boolean getInitState(Context context) {
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);
	return p.getBoolean(context.getString(R.string.keys_prefs_global_init), false);
  }

  /** Sets the application init state after setup. Whipes again on re-setup */
  public static void setInitState(boolean state, Context context) {
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);
	p.edit().putBoolean(context.getString(R.string.keys_prefs_global_init), state).commit();
  }

  /**
   * Sets the selected colors in the Shared Application Preferences. Colors are saved in four
   * different Color Strings to preserve the exact order.
   */
  public static void setColors(ArrayList<String> colors, Context context) {
	if (colors == null)
	  return;
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);
	p.edit().putString(context.getString(R.string.keys_prefs_hashed_color_first), colors.get(0)).commit();
	p.edit().putString(context.getString(R.string.keys_prefs_hashed_color_second), colors.get(1)).commit();
	p.edit().putString(context.getString(R.string.keys_prefs_hashed_color_third), colors.get(2)).commit();
	p.edit().putString(context.getString(R.string.keys_prefs_hashed_color_forth), colors.get(3)).commit();
  }

  /**
   * Assembles an list of colors from the Shared Application Preferences DB base strings. User
   * selected colors in order!
   * 
   * @param context
   * @return
   */
  public static ArrayList<String> getColors(Context context) {
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);

	ArrayList<String> temp = new ArrayList<String>();
	temp.add(p.getString(context.getString(R.string.keys_prefs_hashed_color_first), null));
	temp.add(p.getString(context.getString(R.string.keys_prefs_hashed_color_second), null));
	temp.add(p.getString(context.getString(R.string.keys_prefs_hashed_color_third), null));
	temp.add(p.getString(context.getString(R.string.keys_prefs_hashed_color_forth), null));

	return temp;
  }

  /**
   * Sets the selected objects in the Shared Application Preferences. Objects are being stored in
   * four different Object Strings to preserve order. Color information not included.
   * 
   * @param objects
   * @param context
   */
  public static void setObjects(ArrayList<String> objects, Context context) {
	if (objects == null)
	  return;
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);
	p.edit().putString(context.getString(R.string.keys_prefs_hashed_objects_first), objects.get(0)).commit();
	p.edit().putString(context.getString(R.string.keys_prefs_hashed_objects_second), objects.get(1)).commit();
  }

  /**
   * Returns an ArrayList of strings containing the user-defined Objects. No color-information
   * included. Colors will be selected randomly from the randomly generated batch at setup. To get
   * the exact set of Colors from Setup-creation use {@link #COLORS}
   * 
   * @param context
   * @return
   */
  public static ArrayList<String> getObjects(Context context) {
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);

	ArrayList<String> temp = new ArrayList<String>();
	temp.add(p.getString(context.getString(R.string.keys_prefs_hashed_objects_first), null));
	temp.add(p.getString(context.getString(R.string.keys_prefs_hashed_objects_second), null));

	return temp;
  }

  /**
   * Returns 2 colors that were saved in the setup process for the objects. THIS SHOULD NOT BE USED
   * OUTSIDE SETUP AGENT.
   */
  public static ArrayList<String> getSmallBatchColors(Context context) {
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);

	ArrayList<String> temp = new ArrayList<String>();
	temp.add(p.getString(context.getString(R.string.keys_prefs_batches_small_one), null));
	temp.add(p.getString(context.getString(R.string.keys_prefs_batches_small_two), null));
	return temp;
  }

  /**
   * writes two colors into db in order that they were used in the setup agent. Only used for
   * rendering beauty and consistency.
   */
  public static void setSmallBatchColors(ArrayList<String> batch, Context context) {
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);
	p.edit().putString(context.getString(R.string.keys_prefs_batches_small_one), batch.get(0)).commit();
	p.edit().putString(context.getString(R.string.keys_prefs_batches_small_two), batch.get(1)).commit();
  }

  /**
   * Returns a List with 8 pictures containing a Drawable ID and a color Integer to ensure that the
   * picture and color combinations (Pair<String, String>) are unique.
   */
  public static ArrayList<Pair<String, String>> getBigBatchMap(Context context) {
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);
	ArrayList<Pair<String, String>> result = new ArrayList<Pair<String, String>>();
	// From 0 --> 7 == size = 8
	for (int n = 0; n < 8; n++) {
	  String key = p.getString(context.getString(R.string.batch_big_key) + n, null);
	  String value = p.getString(context.getString(R.string.batch_big_value) + n, null);
	  result.add(new Pair<String, String>(key, value));
	}
	if (result.size() != 8)
	  Log.wtf(AppSettings.class.getSimpleName(), "LIST SIZE OFF! SOMETHING WENT WRONG!");
	return result;
  }

  /** Takes a Pair-List with 8 entries for a Drawable ID and a color Integer. */
  public static void setBigBatchMap(ArrayList<Pair<String, String>> batch, Context context) {
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);
	if (batch.size() != 8)
	  Log.wtf(AppSettings.class.getSimpleName(), "MAP SIZE OFF! ABORTING BATCH WRITING");
	int counter = 0;
	for (Pair<String, String> entry : batch) {
	  String key = entry.first;
	  String value = entry.second;
	  p.edit().putString(context.getString(R.string.batch_big_key) + counter, key).commit();
	  p.edit().putString(context.getString(R.string.batch_big_value) + counter, value).commit();
	  counter++;
	}
	if (counter != 8)
	  Log.wtf(AppSettings.class.getSimpleName(), "MAP SIZE WAS OFF! SOMETHING HAS GONE WRONG. CHECK LOG!");
  }

  /** Sets the bigBatchWrite flag (from the Setup Screen) */
  public static void setBigBatchFlag(boolean batch, Context context) {
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);
	p.edit().putBoolean(context.getString(R.string.keys_prefs_global_bigbatch), batch).commit();
  }

  /** Checks if the bigBatchWrite is enabled and should be used globally */
  public static boolean getBigBatchFlag(Context context) {
	SharedPreferences p = context.getSharedPreferences(context.getString(R.string.keys_prefs_global),
		Context.MODE_PRIVATE);
	return p.getBoolean(context.getString(R.string.keys_prefs_global_bigbatch), false);
  }

  /** Gets a Drawable resource with an Application context and the String saved in the Database */
  public static int getPictureDrawableWithStringId(String id) {
	if (id.equals("Car"))
	  return R.drawable.r2_graph_car;

	else if (id.equals("Hat"))
	  return R.drawable.r2_graph_hat;

	else if (id.equals("Hat2"))
	  return R.drawable.r2_graph_hat2;

	else if (id.equals("Laptop"))
	  return R.drawable.r2_graph_laptop;

	else if (id.equals("Phone"))
	  return R.drawable.r2_graph_phone;

	else if (id.equals("Plane"))
	  return R.drawable.r2_graph_plane;

	else if (id.equals("Shoe"))
	  return R.drawable.r2_graph_shoe;

	else if (id.equals("Umbrella"))
	  return R.drawable.r2_graph_umbrella;

	else
	  return R.drawable.r2_graph_container;
  }

  /** Returns a color with a hex-string */
  public static int getColorWithHex(String hex) {
	if (hex == null) {
	  return Color.argb(0, 0, 0, 0);
	}
	int color = (int) Long.parseLong(hex, 16);
	int r = (color >> 16) & 0xFF;
	int g = (color >> 8) & 0xFF;
	int b = (color >> 0) & 0xFF;
	return Color.argb(200, r, g, b);
  }

}
