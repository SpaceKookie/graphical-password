/** ####################################################################### 
 * Copyright 2014 RANDOM ROBOT SOFTWORKS (see @authors file)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at*
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ####################################################################### */
package de.r2soft.graphicalpassword.core;

import java.util.ArrayList;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import de.r2soft.graphicalpassword.R;
import de.r2soft.graphicalpassword.login.Login1Colors;
import de.r2soft.graphicalpassword.login.Login2Objects;
import de.r2soft.graphicalpassword.login.Login3Fragment;
import de.r2soft.graphicalpassword.setup.SetupContainer;

/**
 * Core activity that wraps around the login procedure, get's run at start-up and triggers setup
 * event.
 * 
 * @author Katharina <katharina.sabel@2rsoftworks.de>
 * 
 */
public class CoreActivity extends Activity {

  private static enum STATE {
	COLORS, PICTURES, FINAL;
  }

  private STATE state;
  private Button leftNavigation, rightNavigation;
  private Login1Colors colorFragment;
  private Login2Objects objFragment;
  private boolean SUCCESS;

  private ArrayList<String> selectedColors, selectedObjects;
  private Menu menu;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.container);

	if (!AppSettings.getInitState(this))
	  startSetup();
	else {
	  colorFragment = new Login1Colors();
	  objFragment = new Login2Objects();

	  leftNavigation = (Button) findViewById(R.id.login_screen_left_button);
	  rightNavigation = (Button) findViewById(R.id.login_screen_right_button);
	  leftNavigation.setOnClickListener(leftButtonClickListener);
	  rightNavigation.setOnClickListener(rightButtonClickListener);

	  showFragment(STATE.COLORS);
	}

  }

  /**
   * Will flush anything that was previously stored in the database and will trigger the setup
   * process.
   */
  private void startSetup() {
	SharedPreferences p = getSharedPreferences(getString(R.string.keys_prefs_global), Context.MODE_PRIVATE);
	p.edit().clear().commit();

	Intent setupIntent = new Intent(this, SetupContainer.class);
	startActivityForResult(setupIntent, 0);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
	/* Remove this to disable the Setup button in the Menu bar. */
	getMenuInflater().inflate(R.menu.core, menu);
	this.menu = menu;

	/*
	 * Comment out this if statement if you always want to be able to run setup. Otherwise the menu
	 * ill only be visible once the app has been unlocked. Which also means, that you need to
	 * manually purge settings if you forget your combination.
	 */
	MenuItem item = menu.findItem(R.id.action_setup);
	item.setVisible(false);

	return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
	int id = item.getItemId();
	if (id == R.id.action_setup) {
	  startSetup();
	  return true;
	}
	return super.onOptionsItemSelected(item);
  }

  private OnClickListener leftButtonClickListener = new OnClickListener() {
	@Override
	public void onClick(View v) {
	  if (state == STATE.PICTURES) {
		showFragment(STATE.COLORS);
	  }
	  else if (state == STATE.FINAL) {
		showFragment(STATE.COLORS);
		SUCCESS = false;
		colorFragment.flush();
		objFragment.flush();
	  }

	}
  };

  private OnClickListener rightButtonClickListener = new OnClickListener() {
	@Override
	public void onClick(View v) {
	  if (state == STATE.COLORS)
		showFragment(STATE.PICTURES);

	  else if (state == STATE.PICTURES)
		showFragment(STATE.FINAL);
	}
  };

  private void showFragment(STATE state) {
	if (this.state == STATE.COLORS) {
	  selectedColors = colorFragment.getSelection();
	}

	/** Writes picture selection to DB */
	if (this.state == STATE.PICTURES) {
	  selectedObjects = objFragment.getSelection();
	}

	this.state = state;
	FragmentTransaction trans = getFragmentManager().beginTransaction();

	if (state == STATE.COLORS) {
	  trans.replace(R.id.login_container, colorFragment);
	}
	else if (state == STATE.PICTURES) {
	  trans.replace(R.id.login_container, objFragment);
	}
	else if (state == STATE.FINAL) {
	  compareData();
	  Bundle bundle = new Bundle();
	  bundle.putBoolean("result", compareData());
	  Login3Fragment fragment = new Login3Fragment();
	  fragment.setArguments(bundle);
	  trans.replace(R.id.login_container, fragment);
	}

	trans.commit();
	updateView();
  }

  private void updateView() {

	if (state == STATE.COLORS) {
	  leftNavigation.setEnabled(false);
	  rightNavigation.setEnabled(true);
	  rightNavigation.setText("Continue");
	}
	else if (state == STATE.PICTURES) {
	  leftNavigation.setEnabled(true);
	  rightNavigation.setEnabled(true);
	  leftNavigation.setText("Cancel");
	  rightNavigation.setText("Submit");
	}
	else if (state == STATE.FINAL) {
	  leftNavigation.setText("Logout");
	  rightNavigation.setEnabled(false);
	  leftNavigation.setEnabled(true);

	}

  }

  private boolean compareData() {

	boolean firstStep = false;
	boolean secondStep = false;

	ArrayList<String> colorsFromDB = AppSettings.getColors(this);
	ArrayList<String> objectsFromDB = AppSettings.getObjects(this);

	if (selectedColors.equals(colorsFromDB))
	  firstStep = true;
	if (selectedObjects.equals(objectsFromDB))
	  secondStep = true;
	if (firstStep && secondStep)
	  SUCCESS = true;

	MenuItem item = menu.findItem(R.id.action_setup);
	item.setVisible(true);
	return SUCCESS ? true : false;
  }

}
